package com.fissara.groupby_tosortedlist;

import org.junit.Test;

import java.util.concurrent.TimeUnit;

import rx.observers.TestSubscriber;

import static com.fissara.groupby_tosortedlist.TestUtils.getTasks;

public class TestJvm {

    @Test
    public void testRun() {
        TestSubscriber<Void> subscriber = new TestSubscriber<>();
        new SyncTaskWorker().executeTasks(getTasks()).subscribe(subscriber);
        subscriber.awaitTerminalEvent(5, TimeUnit.SECONDS);
        subscriber.assertTerminalEvent();
        subscriber.assertNoErrors();
    }
}
