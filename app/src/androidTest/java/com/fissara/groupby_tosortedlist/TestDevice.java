package com.fissara.groupby_tosortedlist;

import android.test.AndroidTestCase;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.observers.TestSubscriber;

import static com.fissara.groupby_tosortedlist.TestUtils.getTasks;

public class TestDevice extends AndroidTestCase {

    public void testRun() {

        TestSubscriber<Void> subscriber = new TestSubscriber<>();
        new SyncTaskWorker().executeTasks(getTasks()).subscribe(subscriber);
        subscriber.awaitTerminalEvent(5, TimeUnit.SECONDS);
        subscriber.assertTerminalEvent();
        subscriber.assertNoErrors();
    }
}
