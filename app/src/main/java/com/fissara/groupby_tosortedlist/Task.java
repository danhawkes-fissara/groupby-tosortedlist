package com.fissara.groupby_tosortedlist;

public class Task {

    private final int priority;
    private final long sequenceNumber;

    public Task(int priority, long sequenceNumber) {
        this.priority = priority;
        this.sequenceNumber = sequenceNumber;
    }

    public int getPriority() {
        return priority;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    @Override
    public String toString() {
        return "Task{" + priority + ", " + sequenceNumber + "}";
    }
}