package com.fissara.groupby_tosortedlist;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Observable.Transformer;

public class TestUtils {

    public static <T> Transformer<T, T> log(String label) {
        return o -> o.doOnEach(o1 -> System.out.println(label + " " + o1.toString()))
                .doOnSubscribe(() -> System.out.println(label + " onSubscribe"))
                .doOnUnsubscribe(() -> System.out.println(label + " onUnsubscribe"));
    }

    public static Observable<Task> getTasks() {
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task(1, 1));
        tasks.add(new Task(2, 1));
        tasks.add(new Task(2, 2));
        tasks.add(new Task(2, 3));
        tasks.add(new Task(2, 4));
        tasks.add(new Task(2, 5));
        tasks.add(new Task(1, 6));
        tasks.add(new Task(2, 0));
        tasks.add(new Task(2, 7));
        tasks.add(new Task(1, 8));
        tasks.add(new Task(1, 9));
        tasks.add(new Task(1, 10));
        tasks.add(new Task(1, 11));
        tasks.add(new Task(2, 12));
        tasks.add(new Task(1, 0));
        tasks.add(new Task(1, 14));
        tasks.add(new Task(1, 15));
        tasks.add(new Task(1, 16));
//        tasks.add(new Task(1, 17));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(1, 0));
//        tasks.add(new Task(2, 0));
        return Observable.from(tasks);
    }
}
