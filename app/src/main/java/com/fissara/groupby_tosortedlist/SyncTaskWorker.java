package com.fissara.groupby_tosortedlist;

import rx.Observable;

import static com.fissara.groupby_tosortedlist.TestUtils.log;

public class SyncTaskWorker {

    public Observable<Void> executeTasks(Observable<Task> tasks) {

        return tasks.groupBy(Task::getPriority)
                .compose(log("group1"))
                .toSortedList((a1, b1) -> Integer.compare(a1.getKey(), b1.getKey()), 1000)
                .compose(log("sorted1"))
                .flatMap(o1 -> Observable.from(o1).concatMap(priorityGroup -> {

                    return priorityGroup.groupBy(Task::getSequenceNumber)
                            .compose(log("group2"))
                            .toSortedList((a2, b2) -> Long.compare(a2.getKey(), b2.getKey()), 1000)
                            .compose(log("sorted2"))
                            .flatMap(o2 -> Observable.from(o2).concatMap(sequenceNumberGroup -> {

                                return sequenceNumberGroup.compose(log("task"))
                                        .ignoreElements()
                                        .cast(Void.class);
                            }));
                }));
    }
}